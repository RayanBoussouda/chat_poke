import React from 'react';

function AboutPage() {
  return (
    <div>
      <h1>About Page</h1>
      <p>Learn more about me and what I do.</p>
    </div>
  );
}

export default AboutPage;