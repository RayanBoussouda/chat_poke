
import React, { useEffect, useState } from 'react';
import ChatBotService from './services';
import {htmlToText} from 'html-to-text'
import './Chat.css';
import Button from '@mui/material/Button';

function Chat() {
  const [pages, setPages] = useState([]);
  const [prompt, setPrompt] = useState([]);
  const [bigChunk, setBigChunk] = useState([]);


  
  async function getPageData(pageId) {
    const response = await ChatBotService.getPages(pageId);
    return response.data
    // setPages([...pages, ...response.data]);
    // getPageContent(pageId);
    
  }
  async function getPageContent(pageId) {
    const response = await ChatBotService.getPageContent(pageId);
    return response.data
    // if(pageId!==12550163){
    //   const response = await ChatBotService.getPageContent(pageId);
    //   setPrompt([...prompt,response.data]);
    // }
   
  }

  useEffect(() => { 
    console.log(process.env.REACT_APP_TEST);
    console.log(process.env);
      Promise.all([
        getPageContent(12550163),
        getPageData(12550163)
      ]).then(([promptData, pagesData]) => {
        setPages([...pages, pagesData]);
        setPrompt([...prompt,promptData])
        const newBigChunk = {
          prompt: promptData,
          pages: pagesData
        };
        setBigChunk([newBigChunk]);
      });      
  }, []);

  function handleAddPage(id) {
    console.log(process.env);
    console.log(process.env.test);

    Promise.all([
      getPageContent(id),
      getPageData(id)
    ]).then(([promptData, pagesData]) => {
      setPages([...pages, pagesData]);
      setPrompt([...prompt,promptData])
      const newBigChunk = {
        prompt: promptData,
        pages: pagesData
      };
      setBigChunk([...bigChunk,newBigChunk]);
    });

  }
  
  return (
    
    <div className="chat-container">
    
      <div className="chat-messages">
        {bigChunk.length > 0 && bigChunk.map((chunkus)=>{
          return(
            <div>
            <div className="message-prompt">
            {/* {chunkus?.prompt.map((prom)=>{
              return (
                <div dangerouslySetInnerHTML={{__html: `${prom}`}} /> 
              )
            })}  */}
            
            <div dangerouslySetInnerHTML={{__html: `${chunkus.prompt}`}} />
            </div>
            {chunkus?.pages.map((page)=>{
             return(<div className="chat-option"><p onClick={() => handleAddPage(page.id)}>{page.title}</p></div>) 
            })
            }           
            </div>
          )
        })          
        }
      </div>
    </div>
  );
}

export default Chat;







  {/* {pages &&
          pages.map((page, index) => (
            <div key={page.id}>
            {index === pages.length - 1 && (
              <div className="message-prompt">
               <div dangerouslySetInnerHTML={{__html: `${prompt}`}} />
              </div>
            )}
            <div className="chat-option"><p onClick={() => handleAddPage(page.id)}>{page.title}</p></div>
            </div>
          ))} */}



// import React, { useEffect, useState } from 'react';
// import ChatBotService from './services';
// import {htmlToText} from 'html-to-text'


// function Chat() {
//   const [pages, setPages] = useState([]);
//   const [prompt, setPrompt]= useState("");
  
//   async function getPageData(pageId) {
//     const response = await ChatBotService.getPages(pageId);
//     setPages([...pages, ...response.data]);
//     getPageContent(pageId);
//   }
//   async function getPageContent(pageId) {
//     const response = await ChatBotService.getPageContent(pageId);
//     const plainText = htmlToText(response.data);
    
//     setPrompt(plainText);
//   }

//   useEffect(() => {
//     getPageContent(12550163);
//     getPageData(12550163);
//   }, []);
//   useEffect(()=>{
//       console.log(pages)
//   },[pages])
//   function handleAddPage(id) {
//     getPageData(id)
//   }
//   return (
//     <div>
      
//       {pages &&
//         pages.map((page,index) => (  
//           <div>
              
//             <p key={page.id} onClick={() => handleAddPage(page.id)}>
//             {page.title}
//           </p>
//           {/* {index===pages.length-1 &&<p>{prompt}</p>    }   */}
//          </div>
//         ))}
//     </div>
//   );
// }

// export default Chat;