import axios from 'axios';

async function getPageData() {
  const response = await ChatBotService.getPages();
  console.log(response.data);
}


const ChatBotService = {
  getPages(pageId) {
    return axios.get(`http://localhost:3006/getPageDirectChildren/${pageId}`);
  },
  getPageContent(pageId) {
    return axios.get(`http://localhost:3006/getPageContent/${pageId}`);
  }
}

export default ChatBotService;

