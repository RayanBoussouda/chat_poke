import React from 'react';

function HomePage() {
  console.log(process.env.test);
  return (
    <div>
      <h1>Home Page</h1>
      <p>Welcome to my home page!</p>
    </div>
  );
}

export default HomePage;