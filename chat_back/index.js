const express = require('express');
const axios = require('axios');
const cheerio = require('cheerio');
const https = require('https');
const app = express();
const port = 3006;
const email = 'thomas.parance@acensi.fr'

const spaceId = 8749083
const pageId = 12550163
const cors = require('cors');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");

  next();
});
app.use(
  cors({
    origin: ['http://localhost:3000', 'http://localhost:8080']
  })
);
const headers = {
  headers: {
    'Authorization': `Basic ${Buffer.from(
      `${email}:${token}`
    ).toString('base64')}`,
    'Accept': 'application/json'
  }
}
app.get('/getSpaces', (req, res) => {
  axios.get('http://crpratp.atlassian.net/wiki/api/v2/spaces',headers )
    .then(response => {
      res.send(response.data.results);
    })
    .catch(error => {
      console.error(error);
      res.status(500).send('Error retrieving chat data');
    });
});

app.get('/getSpaceById', (req, res) => {
  axios.get(`http://crpratp.atlassian.net/wiki/api/v2/spaces/${spaceId}/pages`,headers )
    .then(response => {
      res.send(response.data.results);
    })
    .catch(error => {
      console.error(error);
      res.status(500).send('Error retrieving chat data');
    });
});

app.get('/getPageDirectChildren/:pageId', (req, res) => {
  const pageId = req.params.pageId;
  axios.get(`http://crpratp.atlassian.net/wiki/api/v2/pages/${pageId}/children`,headers )
    .then(response => {
      res.send(response.data.results);
    })
    .catch(error => {
      console.error(error);
      res.status(500).send('Error retrieving chat data');
    });
});

// app.get('/getPageContent/:pageId', (req, res) => {
//   const pageId = req.params.pageId;
//   axios.get(`https://crpratp.atlassian.net/wiki/rest/api/content/${pageId}?expand=body.storage`,headers )
//     .then(response => {     
//       res.send(response.data.body.storage.value);
//     })
//     .catch(error => {
//       console.error(error);
//       res.status(500).send('Error retrieving chat data');
//     });
// });

// app.get('/getPageDirectChildren', (req, res) => {
//   axios.get(`http://crpratp.atlassian.net/wiki/api/v2/pages/12550163/children`,headers )
//     .then(response => {
//       res.send(response.data.results);
//     })
//     .catch(error => {
//       console.error(error);
//       res.status(500).send('Error retrieving chat data');
//     });
// });

app.get('/testlink', async (req, res) => {
  try {
    const agent = new https.Agent({ rejectUnauthorized: false });
    const link = "https://www.crpratp.fr/web/actif/dal/union-retraite";
    const titleResponse = await axios.get(link, { httpsAgent: agent });
    console.log(titleResponse.status);
    // console.log(titleResponse.data);
    // console.log(titleResponse)
    res.send(titleResponse.data)
  } catch (error) {
    // console.log(error)
    res.send(error)
  }
});


app.get('/getPageContent/:pageId', async (req, res) => {
  const pageId = req.params.pageId;
  try {
    const response = await axios.get(`https://crpratp.atlassian.net/wiki/rest/api/content/${pageId}?expand=body.storage`, headers);
    const html = response.data.body.storage.value;
    const $ = cheerio.load(html);
    const link = $('a').attr('href');
    if (link) {
      const agent = new https.Agent({ rejectUnauthorized: false });
      const titleResponse = await axios.get(link, { httpsAgent: agent });
      const titleHtml = titleResponse.data;
      const titleRegex = /<title>(.*?)<\/title>/;
      const matches = titleHtml.match(titleRegex);
      const title = matches ? matches[1] : '';
      $('a').attr('href', link).text(title);
      const newHtml = $.html();
      console.log(newHtml)
      res.send(newHtml);
    } else {
      res.send(html);
    }
  } catch (error) {
    console.error(error);
    res.status(500).send('Error retrieving chat data');
  }
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

